export interface BulkMessagesPayload {
    messages: MessageMap
}

export interface MessageData {
    author:  string
    color:   number
    content: string
}

export interface MessageMap {
    [ID: string]: MessageData 
}