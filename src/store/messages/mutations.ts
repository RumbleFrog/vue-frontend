import { MutationTree } from 'vuex';
import { Message, MessageState } from './types';

export const mutations: MutationTree<MessageState> = {
	addMessage(state, payload: Message) {
		state.Messages.set(payload.ID, payload);
	},
}
