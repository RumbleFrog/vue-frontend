import { User } from '../user/types';

export interface Message {
	ID: string;
	Author: User;
	Color: number;
	Content: string;
	// Possible add reactions
}

export interface MessageState {
	Messages: Map<string, Message>;
}
