import { GetterTree } from 'vuex';
import { MessageState } from './types';
import { RootState } from '../types';

export const getters: GetterTree<MessageState, RootState> = {
	size(state): number {
		return state.Messages.size;
	},
}
