export interface User {
	name: string;
	id: string;
	createdAt: Date;
}
